package pompackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.SeleniumFunctions;

public class LoginPage {
	public WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	SeleniumFunctions seleniumFunc = new SeleniumFunctions();
	
	@FindBy(linkText = "SIGN-ON")
	private WebElement SIGNON;
	
	@FindBy(xpath = "//img[@src='images/mast_signon.gif']")
	private WebElement titleSignOn;
	
	@FindBy(name = "userName")
	private WebElement userName;
	
	@FindBy(name = "password")
	private WebElement password;
	
	@FindBy(name = "submit")
	private WebElement submit;
	
	@FindBy(tagName =  "h3")
	private WebElement loginSuccess;
	//==================================
	
	
	public void clickSignOn() {
		//SIGNON.click();
		seleniumFunc.clickElement(SIGNON);
	}
	
	public void VerifySignOnTitle() {
		clickSignOn() ;
		
		seleniumFunc.verifyPageTitle(SIGNON);
	}
	
	
	public void loginIntoApplication(String username, String passwrod) {
		enterUserName( username) ;
		enterPassword( passwrod) ;
		clickSubmit() ;
		
	}
	
	public void VerifyLoginSuccessfull() {
		seleniumFunc.waitElementToBeDisplayed(driver ,loginSuccess);
		Assert.assertEquals("Login Successfully", loginSuccess.getText());
	}
	
	public void enterUserName(String val) {
//		userName.sendKeys(val);
		seleniumFunc.waitElementToBeDisplayed(driver ,userName);
		seleniumFunc.enterValueIntoTextField(userName, val);
	}
	public void enterPassword(String val) {
		//password.sendKeys(val);
		seleniumFunc.enterValueIntoTextField(password, val);
	}
	
	public void clickSubmit() {
		// submit.click();
		seleniumFunc.clickElement(submit);
	}
}
