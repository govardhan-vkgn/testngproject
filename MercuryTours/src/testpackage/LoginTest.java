package testpackage;

import java.io.IOException;
import org.testng.annotations.Test;

import pompackage.LoginPage;
import utilities.ReadPropertyFile;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LoginTest extends BaseClass{
		
	//@Test(dataProvider = "testData")
	public void VerifyLoginPage() {
		/*		Step1 launch the browser   -- Step2:	Verify title of the login      */
		test.log(LogStatus.INFO, "Test Case started");
		LoginPage loginpage = new LoginPage(driver);
		loginpage.VerifySignOnTitle();
		test.log(LogStatus.PASS, "Verified the Sign On Title");
		test.log(LogStatus.INFO, "Test Case is passed");
	}
	
	
	@Test
	public void VerifySuccessfullLogin() throws IOException {
		
		/* 		Step1 launch the browser --		Verify title of the login
		Step 2: Enter username and password then click on submit  -- 	Verify login successfull   	*/
		test.log(LogStatus.INFO, "Test Case started");
		ReadPropertyFile config = new ReadPropertyFile();
		String userName = config.getuserName();
		String password = config.getPassword();
		LoginPage loginpage = new LoginPage(driver);
		loginpage.VerifySignOnTitle();
		test.log(LogStatus.PASS, "Verified the Sign On Title");
		loginpage.loginIntoApplication(userName, password);
		loginpage.VerifyLoginSuccessfull();
		test.log(LogStatus.PASS, "Login is completed");
		test.log(LogStatus.INFO, "Test Case is passed");
	}

	
}
