package testpackage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.excelant.ExcelAntSet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utilities.ReadExcel;
import utilities.ReadPropertyFile;

public class BaseClass {
	

	public WebDriver driver;
	static ExtentTest test;
	static ExtentReports report;
	
	ReadExcel readexcel = new ReadExcel();

	String xlFilePath = "File/TestData.xlsx";
    String sheetName = "data";
   
    @DataProvider(name="testData")
    public Object[][] userFormData() throws Exception
    {
        Object[][] data = readexcel.getTableArray(xlFilePath, sheetName);
        return data;
    }
    
    
    
	@BeforeMethod
	public void launchApplication() throws IOException {
		
		report = new ExtentReports(System.getProperty("user.dir")+"ExtentReportResults.html");
		test = report.startTest("ExtentDemo");
		
		ReadPropertyFile config = new ReadPropertyFile();
		
		String url = config.getUrl();
		String browserType = config.getBrowserType();
		
		if(browserType.equalsIgnoreCase("CHROME")) {
			System.setProperty("webdriver.chrome.driver","drivers/chromedriver");  
			
			driver = new ChromeDriver();
			
			driver.get(url);
			test.log(LogStatus.INFO, "Application is launched");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();	
		}else if(browserType.equalsIgnoreCase("FireFox")){
			
			System.setProperty("webdriver.gecko.driver","drivers/geckodriver" );  
			driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();	
		}else if (browserType.equalsIgnoreCase("SAFARI")) {
			driver = new SafariDriver();  
			driver.get(url);
		}	
	}
	
	
	@AfterMethod
	public void closerBrowser() {
		driver.close();
	}
	
}
