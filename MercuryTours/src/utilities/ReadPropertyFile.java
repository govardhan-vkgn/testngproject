package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadPropertyFile {
	
	Properties prop ;
  
    public ReadPropertyFile() throws IOException  {
    	FileInputStream fis = new FileInputStream("/Users/kgovardhan/git/testngproject/MercuryTours/config/config1.properties");
    	prop = new Properties();
        prop.load(fis);       
    }   
    
    public  String getUrl() {
    	return prop.getProperty("url");
    }
    
    public  String getuserName() {
    	return prop.getProperty("userName");
    }
    
    public  String getPassword() {
    	return prop.getProperty("password");
    }
    
    public  String getBrowserType() {
    	return prop.getProperty("Browser");
    }
    
}
