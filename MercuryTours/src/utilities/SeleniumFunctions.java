package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SeleniumFunctions {

	public void clickElement(WebElement ele) {
		ele.click();
	}
	
	public void enterValueIntoTextField(WebElement ele , String val) {
		ele.sendKeys(val);
	}
	
	public void selectValueFromDropDown(WebElement ele , String val) {
		ele.sendKeys(val);
	}
	
	public void verifyPageTitle(WebElement ele) {
		Assert.assertTrue(ele.isDisplayed());
	}
	
	
	public void waitElementToBeDisplayed(WebDriver driver , WebElement ele) {
		
		WebDriverWait w = new WebDriverWait(driver, 20);
		w.until(ExpectedConditions.invisibilityOfElementLocated((By) ele));
		
	}
}
