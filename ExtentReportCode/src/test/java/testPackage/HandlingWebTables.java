package testPackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class HandlingWebTables {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/test/web-table-element.php");
		
		WebElement tableEle = driver.findElement(By.xpath("//table[@class='dataTable']"));
		
		if(tableEle.isDisplayed() == true ) {
			
			 List<WebElement> columns = driver.findElements(By.xpath("//table[@class='dataTable']/thead/tr/th"));
			
			 System.out.println("Total columns : "+columns.size());
			 List<WebElement> rows = driver.findElements(By.xpath("//table[@class='dataTable']/tbody/tr"));
			 System.out.println("Total rows : "+rows.size());
			
/*
				System.out.println();
			 System.out.println("Column Hearders : ");
			for (WebElement column:columns) {
				String colText = column.getText();
				System.out.println(colText);
			}
			 
			System.out.println();
			System.out.println("Row text : ");
			for(WebElement row:rows) {
				String rowtext = row.getText();
				System.out.println(rowtext);
			}
			
			*/
			
			 
			 
			 int all = checkTheTotalCompanies(driver, rows);
			 Assert.assertEquals(26, all);
	
			 checkTheTotalCompanies(driver, rows);
			 ArrayList<String> allCompanyNames  = getTotalCompaniesInNSE(driver, rows);
			 
		}
		
		
		Thread.sleep(5000);
		driver.close();
		
		
	}
	
	public static int checkTheTotalCompanies(WebDriver driver ,List<WebElement>rows){
		return rows.size();
	}
	
	
	
	public static ArrayList<String> getTotalCompaniesInNSE(WebDriver driver ,List<WebElement>rows){
		 ArrayList<String> allCompanyNames = new ArrayList<String>();
			
		 int rowcount = 1;
		
		 for(WebElement row:rows) {
			// System.out.println(row.getSize() );
			String xpath = "//table[@class='dataTable']/tbody/tr["+ rowcount +"]/td[1]/a";
			String comapnyname = driver.findElement(By.xpath(xpath)).getText();
			allCompanyNames.add(comapnyname);				
			rowcount++;
		 }
		 
		 System.out.println("Before Sort : " + allCompanyNames);
		 Collections.sort(allCompanyNames);
		 System.out.println("After sor :" + allCompanyNames);
		 
		return allCompanyNames;
	}
	 
	

}
