package testPackage;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import java.time.*;
import java.time.Month;

public class DatePicker {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demos.telerik.com/kendo-ui/datetimepicker/index");

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	
	   Format f = new SimpleDateFormat("dd/MMMM/yyyy");
	   String strDate = f.format(new Date());
	   f = new SimpleDateFormat("yyyy");
	   
	   String strYear = f.format(new Date());
	   int year1 = Integer.parseInt(strYear)-1;
	   
	   f = new SimpleDateFormat("MMM");
	      String strMonth = f.format(new Date());
	      
	      f = new SimpleDateFormat("dd");
	      String day = f.format(new Date());
	      int day1 = Integer.parseInt(day)-1;

		driver.findElement(By.xpath("//span[@class='k-icon k-i-calendar k-button-icon']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@data-action='nav-up']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@data-action='nav-up']")).click();
		
		Thread.sleep(3000);
		
	   
	   String yearxpath = "//a[text()='"+ year1 +"']";
		
	   driver.findElement(By.xpath(yearxpath)).click();
	   Thread.sleep(3000);
	  String monthxpath = "//a[text()='"+ strMonth +"']";
	   
	  
	  
	  
	  
	  driver.findElement(By.xpath(monthxpath)).click();
	  Thread.sleep(3000);
	   
	  String dayxpath = "(//a[text()='"+ day1 +"'])[1]";
			  
	  driver.findElement(By.xpath(dayxpath)).click();
	  Thread.sleep(3000);
			  System.out.println("wait");
			  
	}

}
