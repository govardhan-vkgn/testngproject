package testPackage;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Sample {
	public static void main(String args[]) throws InterruptedException, AWTException {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demo.guru99.com/V4/index.php");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		
		String TitleName = jse.executeScript("return document.title;").toString();
		
		System.out.println(TitleName);
		
		WebElement userName = driver.findElement(By.name("uid"));
		// userName.sendKeys("admin");
		//jse. executeScript("document.getElementById('firstName'). value='testuser'");
		
		jse.executeScript("arguments[0].value='admin123'", userName); 
		 String val = (String) jse.executeScript("return arguments[0].value", userName);
		
		System.out.println(val);
		
		
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("admin");
		
		// Thread.sleep(5000);
		
		
		jse.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 10000);");

		WebElement btnReset = driver.findElement(By.name("btnReset"));
		
		//btnReset.click();
	
		jse.executeScript("arguments[0].click();", btnReset);
		
		jse.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 10000);");

		jse.executeScript("window.scrollBy(100,600)");

		jse.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 10000);");

		
		Robot robot = new Robot(); 

		robot.keyPress(KeyEvent.VK_CONTROL);	
		Thread.sleep(2000);	
		robot.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_TAB);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(10000);
	}
	
	
	

}
