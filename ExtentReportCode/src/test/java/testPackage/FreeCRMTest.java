package testPackage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pagesPackage.PracticeFormPage;


public class FreeCRMTest extends BaseClass{

	@Test
	public void TC01_VerifyfreeCRMTitle(){
		//extentTest = extent.startTest("TC01_VerifyfreeCRMTitle");
		extentTest.log(LogStatus.PASS, "Browser is launched");
		String title = driver.getTitle();
		System.out.println(title);
		extentTest.log(LogStatus.INFO, "Captured the title");
		Assert.assertEquals(title,"#1 Free CRM customer relationship management software cloud");
		extentTest.log(LogStatus.PASS, "Title is matched");
	}
	
	@Test
	public void freemCRMLogoTest(){
		//extentTest = extent.startTest("freemCRMLogoTest");
		boolean b = driver.findElement(By.xpath("//a[@title='free crm home123']")).isDisplayed();
		Assert.assertTrue(b);
	}

	
	@Test
	public void VerifyInputBox() {
		
		PracticeFormPage practicepage = new PracticeFormPage(driver);
		// practicepage.clicpractiverFormPage();
		extentTest.log(LogStatus.PASS, "CLicked Form Page");
		
	}
}
