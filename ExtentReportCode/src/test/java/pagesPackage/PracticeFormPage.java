package pagesPackage;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PracticeFormPage {

	
	public WebDriver driver;
	
	
	@FindBy(xpath = "//input[@id='subjectsInput']")
	private WebElement subject;
	
	@FindBy(xpath = "//input[@id='react-select-3-input']")
	private WebElement city1;
	
	@FindBy(xpath = "//input[@id='react-select-4-input']")
	private WebElement city2;
	
	
	public PracticeFormPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void subject() throws InterruptedException {
		subject.sendKeys("Test123");
		Thread.sleep(2000);
	}
	
	public void city1() throws InterruptedException {
		Thread.sleep(2000);
		city1.sendKeys("Haryana");
		Thread.sleep(2000);
		city1.sendKeys(Keys.DOWN , Keys.ENTER);
		Thread.sleep(2000);
	}
	
	public void city2() throws InterruptedException {
		Thread.sleep(2000);
		city2.sendKeys("Karnal");
		Thread.sleep(2000);
		city2.sendKeys(Keys.DOWN , Keys.ENTER);
		Thread.sleep(2000);
	}
}
