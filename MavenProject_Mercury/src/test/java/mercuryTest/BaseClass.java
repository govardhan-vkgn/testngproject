package mercuryTest;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import freemarker.template.SimpleDate;
import utilities.ReadExcel;
import utilities.ReadPropertyFile;

public class BaseClass{

	public WebDriver driver;
	
	static ExtentTest test;
	static ExtentReports report;
	
	
/*
	ReadExcel readexcel = new ReadExcel();

	String xlFilePath = "File/TestData.xlsx";
	String sheetName = "data";

	@DataProvider(name = "testData")
	public Object[][] userFormData() throws Exception {
		Object[][] data = readexcel.getTableArray(xlFilePath, sheetName);
		return data;
	}
*/
	
	@BeforeTest
	public static void reports_Start() {
		System.out.println(System.getProperty("user.dir")+"/test-output/ExtentReports.html");
		
		report = new ExtentReports("/Users/kgovardhan/git/testngproject/MavenProject_Mercury/test-output/ExtentReportResults.html");
		report.addSystemInfo("Host Name","Gova Mac");
		report.addSystemInfo("User name" , "Name");
		report.addSystemInfo("Environment","QA");
		
		
		// report.loadConfig(new File ("/Users/kgovardhan/git/testngproject/MavenProject_Mercury/extent-config.xml"));
		// test = report.startTest("ExtentDemo");
	}

	@AfterTest
	public static void endTest(ITestResult result) {
		
		report.flush();
		report.close();
	}
	
	
	public static String getScreenShot(WebDriver driver,String screenShotName) throws IOException {
		String dateFormatName = new SimpleDateFormat("yyyyMMddhh").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destlocation = System.getProperty("user.dir") + "/FailedScreenShot/"+screenShotName+dateFormatName + ".png";
		File finalPath = new File(destlocation);
		FileUtils.copyFile(source, finalPath);	
		
		
		return destlocation;
		
	}
	
	
	
	

	@BeforeMethod
	public void launchApplication() throws IOException {

		ReadPropertyFile config = new ReadPropertyFile();

		String url = config.getUrl();
		String browserType = config.getBrowserType();

		if (browserType.equalsIgnoreCase("CHROME")) {
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

			driver = new ChromeDriver();

			driver.get(url);
			test.log(LogStatus.INFO, "Application is launched");
			driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.manage().window().maximize();
			test.log(LogStatus.PASS, "Browser is launched successfully");
		} else if (browserType.equalsIgnoreCase("FireFox")) {

			System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
			driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} else if (browserType.equalsIgnoreCase("SAFARI")) {
			driver = new SafariDriver();
			driver.get(url);
		}
	}

	@AfterMethod
	public void closeBrowser(ITestResult result) throws IOException {
		
		if(result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, "Test Case Fail" + result.getName());
			test.log(LogStatus.FAIL, result.getThrowable());
			
			String screenshotpath = BaseClass.getScreenShot(driver, result.getName());
			test.log(LogStatus.FAIL, test.addScreencast(screenshotpath));
		}
		else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test Case Skip" + result.getName());
		//	test.log(LogStatus.FAIL, result.getThrowable());
		}
		
		report.endTest(test);
		
		driver.close();
		driver.quit();
	}

}
