package testCasePKG;
import org.testng.annotations.Test;
import pomPKG.HomePage;
import pomPKG.LoginPage;
import utilities.ReadPropertyFile;

public class LoginTest extends BaseClass{

	ReadPropertyFile readprop = new ReadPropertyFile();
//Govardhan
  @Test  
  public void LoginWithMobileNUmber() throws InterruptedException {	 
	  String username = readprop.getUserNameAsNumber();
	  String password = readprop.getPassword();
	  
	  LoginPage login = new LoginPage(driver);	
	  HomePage homepage = new HomePage(driver);
	  
	  login.verifyLoginPage();
	  login.enterUserName(username);
	  login.enterPassword(password);
	  login.clickOnLogin();
	  homepage.verifyHomePage();
  }
  
  @Test  
  public void chatWithExistingContact() throws InterruptedException {
	  String username = readprop.getUserNameAsNumber();
	  String password = readprop.getPassword();
	  
	  LoginPage login = new LoginPage(driver);	
	  HomePage homepage = new HomePage(driver);
	  
	  login.verifyLoginPage();
	  login.enterUserName(username);
	  login.enterPassword(password);
	  login.clickOnLogin();
	  homepage.verifyHomePage();
	  homepage.clickOnSearchName();
	  homepage.enterSearchName();
	  homepage.selectSearchName();
	  
  }
  
  @Test 
  public void LoginWithMailID( ) throws InterruptedException {
	  String username = readprop.getUserNameAsMailID();
	  String password = readprop.getPassword();
	  
	  LoginPage login = new LoginPage(driver);	
	  HomePage homepage = new HomePage(driver);
	  
	  login.verifyLoginPage();
	  login.enterUserName(username);
	  login.enterPassword(password);
	  login.clickOnLogin();
	  homepage.verifyHomePage();
  }
  
  @Test
  public void LoginWithInvalidMobileNUmber() throws InterruptedException {
	  
	  String username = readprop.getUserNameAsNumber();
	  String password = readprop.getPassword();
	  
	  LoginPage login = new LoginPage(driver);	
	  HomePage homepage = new HomePage(driver);
	  
	  login.verifyLoginPage();
	  login.enterUserName(username);
	  login.enterPassword(password);
	  login.clickOnLogin();
	  homepage.verifyHomePage();
  }
  
  @Test
  public void LoginWithInvalidMailID( ) throws InterruptedException {
	  String username = readprop.getUserNameAsMailID();
	  String password = readprop.getPassword();
	  
	  LoginPage login = new LoginPage(driver);	
	  HomePage homepage = new HomePage(driver);
	  
	  login.verifyLoginPage();
	  login.enterUserName(username);
	  login.enterPassword(password);
	  login.clickOnLogin();
	  homepage.verifyHomePage();
  }
  
}
