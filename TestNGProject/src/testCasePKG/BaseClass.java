package testCasePKG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import utilities.ReadPropertyFile;

public class BaseClass {

	public WebDriver driver;
	static ExtentTest test;
	static ExtentReports report;
	
	@BeforeMethod
	public void launchFBApplication() {
		
		report = new ExtentReports(System.getProperty("user.dir")+"ExtentReportResults.html");
		test = report.startTest("ExtentDemo");
		
		ReadPropertyFile readprop = new ReadPropertyFile();
		String url = readprop.getURL();
		System.out.println("Initilizing the driver method");
		
		System.setProperty("webdriver.chrome.driver","drivers/chromedriver");  
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();	
	}
	
	public void launchAndhraGreensApplication(String url) {
		System.out.println("Initilizing the driver method");
		
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver");  
		driver = new ChromeDriver();
		driver.get(url);
		driver.findElement(By.xpath(""));
	}

	@AfterMethod
	public void closeBrowser() {
		System.out.println("closing the driver");
		
		driver.close();
		//driver.quit();
		
	}
	
	
	
	
	
	
	
	
	/*
	
	@BeforeSuite
	public void before_suite() {
		System.out.println("executed before suite method");
	}
	
	@BeforeTest
	public void before_Test() {
		System.out.println("executed beore Test method");
	}
	
	@BeforeClass
	public void before_class() {
		System.out.println("executed before class method");
	}

	


	@AfterClass
	public void after_class() {
		System.out.println("executed After class method");
	}

	
	@AfterTest
	public void After_Test() {
		System.out.println("executed After Test method");
	}

	
	@AfterSuite
	public void after_suite() {
		System.out.println("executed After suite method");
	}

	*/

}
