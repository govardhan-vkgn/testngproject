package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadPropertyFile {
	
	Properties prop ;
     
    public void readPropertyFile() throws IOException {
    	FileInputStream fis = new FileInputStream("/Users/kgovardhan/git/testngproject/TestNGProject/config/config.properties");
    	prop = new Properties();
        prop.load(fis);       
    }    
   
    public String getURL() {
    	return prop.getProperty("url");
    }
    
    public String getPassword() {
    	return prop.getProperty("password");
    }
    
    public String getUserNameAsNumber() {
    	return prop.getProperty("userNameNumber");
    }
    
    public String getUserNameAsMailID() {
    	return prop.getProperty("userNameMail");
    }
    
}
