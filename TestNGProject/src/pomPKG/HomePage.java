package pomPKG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {

	WebDriver driver;
	
	@FindBy(xpath = "//span[text()='Eswara Sai']")
	private WebElement homepage;
	
	@FindBy(xpath = "//div[@aria-label='Search by name or group']")
	private WebElement searchName;
		
	@FindBy(xpath="//input[@id='jsc_c_kh']")
	private WebElement enterSearchName;	
			
	@FindBy(xpath = "(//span[text()='US Naveen Kumar'])[1]")
	private WebElement selectChatName;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void verifyHomePage() {
		Assert.assertTrue(homepage.isDisplayed());
	}
	
	public void clickOnSearchName() {
		searchName.click();
	}
		
	public void enterSearchName() throws InterruptedException {
		enterSearchName.sendKeys("US naveen kumar");
		Thread.sleep(5000);
	}
	
	public void selectSearchName() {
		selectChatName.click();
	}
}
