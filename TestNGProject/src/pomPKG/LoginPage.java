package pomPKG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
public class LoginPage {
	
	WebDriver driver;
	
	By verifyLogin = By.cssSelector("img[alt='Facebook']");
	By username = By.cssSelector("input#email");
	By password = By.cssSelector("input[name='pass']");
	By login = By.xpath("//button[@name='login']");
	
	
//	@FindBy(xpath="//input[@id='email']")
//	private WebElement login1;

	//=====================================================
	
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void verifyLoginPage() {
		Assert.assertTrue(driver.findElement(verifyLogin).isDisplayed());
	}
	
	public void enterUserName(String val) throws InterruptedException {
		driver.findElement(username).sendKeys(val);
		Thread.sleep(3000);
	}
	
	public void enterPassword(String val) throws InterruptedException {
		driver.findElement(password).sendKeys(val);
		Thread.sleep(3000);
	}
	
	public void clickOnLogin() throws InterruptedException {
		driver.findElement(login).click();
		Thread.sleep(8000);
	}
	
	
}
